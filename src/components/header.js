import React from 'react'
import { Link } from 'gatsby'
import './header.css'

const Header = ({ siteTitle }) => (
  <div
    style={{
      marginBottom: '1.45rem',
    }}
  >
    <div className="my-header">
      <h1 style={{ margin: 0 }}>
        <Link to="/" className="header-link">
          {siteTitle}
        </Link>
      </h1>
    </div>
  </div>
)

export default Header
