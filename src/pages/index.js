import React from 'react'
import { Link } from 'gatsby'
import { graphql } from 'gatsby'
import Layout from '../components/layout'

export default ({ data }) => (
  <Layout>
    <div
      style={{
        backgroundColor: '#596275',
        padding: '1.8em',
        margin: '5em auto',
        textAlign: 'center',
      }}
    >
      <p style={{ padding: '0em 2em' }}>Software Developer. Life Enthusiast.</p>
      <a
        href={data.allFile.edges[0].node.publicURL}
        style={{ color: '#e15f41', padding: '0em 0.5em' }}
      >
        resume
      </a>
      <Link to="/work/" style={{ color: '#e15f41', padding: '0em 0.5em' }}>
        work
      </Link>
      <Link to="/bio/" style={{ color: '#e15f41', padding: '0em 0.5em' }}>
        bio
      </Link>
      <a
        href="https://github.com/galarus/"
        style={{ color: '#e15f41', padding: '0em 0.5em' }}
      >
        github
      </a>
<a
        href="https://gitlab.com/galarus/"
        style={{ color: '#e15f41', padding: '0em 0.5em' }}
      >
        gitlab
      </a>
      <Link to="/contact/" style={{ color: '#e15f41', padding: '0em 0.5em' }}>
        contact
      </Link>
    </div>
  </Layout>
)

export const query = graphql`
  query {
    allFile(filter: { extension: { eq: "pdf" } }) {
      edges {
        node {
          publicURL
        }
      }
    }
  }
`
