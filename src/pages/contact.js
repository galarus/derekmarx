import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'

const Contact = () => (
  <Layout>
    <div
      style={{
        backgroundColor: '#596275',
        padding: '1.8em',
        margin: '5em auto',
        textAlign: 'center',
      }}
    >
      <h3>For consultation or questions, sms or email me at:</h3>
      <p>derekwmarx@hotmail.com</p>
      <p>(831) 239-2429</p>
      <Link to="/" style={{ color: '#c44569' }}>
        Go back to the homepage
      </Link>
    </div>
  </Layout>
)

export default Contact
