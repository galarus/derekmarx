import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'

const Bio = () => (
  <Layout>
    <div
      style={{
        backgroundColor: '#596275',
        padding: '1.8em',
        margin: '5em auto',
        textAlign: 'center',
      }}
    >
      <h1>Bio</h1>
      <p style={{ padding: '0em 2em' }}>
        <span style={{ paddingLeft: '1em' }} />
        Born in Sacramento, I've lived in California all my life. While growing
        through highschool, I experienced many enlightening moments. Several
        times I have gone to Europe; I did the Eagle Scout thing(favorite merit
        badges: shotgun and wilderness survival), played years of clarinet, and
        have studied classical Latin and Greek languages. Programming is one of
        my main hobbies, and have graduated with a Bachelors of Arts in Computer Science from UCSC.
      </p>
      <Link to="/" style={{ color: '#c44569' }}>
        Go back to the homepage
      </Link>
    </div>
  </Layout>
)

export default Bio
