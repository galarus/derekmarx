import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'

const Work = () => (
  <Layout>
    <div
      style={{
        backgroundColor: '#596275',
        padding: '1.8em',
        margin: '5em auto',
        textAlign: 'center',
      }}
    >
      <h1>Work</h1>
      <a
        href="http://dragoncircle.top"
        style={{ color: '#e15f41', padding: '0em 0.5em' }}
      >
        DragonCircle
      </a>
      <p>
        Multi-user chat rooms, oriented for tabletop RPGs (like D&D). Webcams,
        custom dice roller, chat commands, and special mute controls for the
        room creator. In this project I learned and practiced AGILE and SCRUM
        while gaining experience leading several people to help.
      </p>
      <br />
      <a
        href="https://galarus.github.io/plapp/"
        style={{ color: '#e15f41', padding: '0em 0.5em' }}
      >
        Plant App
      </a>
      <p>
        Search for plants by their morphology. The plant data is from Santa Cruz
        with collaboration from the creator of the UCSC Wildlife app and UCSC
        Natural Reserves.
      </p>
      <br />
      <Link to="/" style={{ color: '#c44569' }}>
        Go back to the homepage
      </Link>
    </div>
  </Layout>
)

export default Work
